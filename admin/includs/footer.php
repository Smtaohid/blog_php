<footer class="py-4 bg-light mt-auto">
                    <div class="container-fluid px-4">
                        <div class="d-flex align-items-center justify-content-between small">
                            <div class="text-muted"> Copyright &copy; Tech Learn <?php echo date("Y"); ?> </div>
                            <p>
                               Design & Developed By 
                                <a href="#">Tech Learn </a>
                            </p>
                        </div>
                    </div>
                </footer>