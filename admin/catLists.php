<?php

include('../Controller/DatabaseFunction.php');



    $query = "SELECT * FROM categories ";
 $functionObj = new DatabaseFunction();
$results = $functionObj->index($query);

if (isset($_POST['delete'])){
    $cat_id = $_POST['cate_id'];
    $deleteQuery = "DELETE FROM categories WHERE id=$cat_id";
    $functionObj->delete($deleteQuery);
    header('Location:catLists.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php

    include('includs/head.php');

    ?>
</head>

<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <?php

    include('includs/nav.php');

    ?>
</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <?php include('includs/site_nav.php'); ?>
    </div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4"> Category Lists</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active"> Category List</li>
                </ol>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="card shadow">
                                <div class="card-header ">
                                    <div class="d-flex justify-content-between">
                                        <h3> Category List</h3>
                                        <a href="catCreate.php" class="btn  btn-success"><i class="fas fa-plus"></i></a>
                                    </div>

                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered ">

                                       <thead>
                                       <tr class="text-center align-middle">
                                           <th>SL</th>
                                           <th>Category Name</th>
                                           <th>Action</th>
                                       </tr>
                                       </thead>

                                         <tbody>
                                         <?php
                                         $sl = 1;
                                         foreach ($results as $list){
                                         ?>
                                       <tr class="text-center align-middle">
                                           <td><?php echo $sl++ ?></td>
                                           <td><?php echo $list->name;?></td>
                                           <td class="d-flex justify-content-center">
                                               <a href="catEdit.php?id=<?php echo $list->id ?>" class="btn btn-warning btn-sm text-white me-2" ><i class="fas fa-edit"></i></a>
                                               <form action="" method="post">
                                                   <input type="hidden" name="cate_id" value="<?php echo $list->id; ?>" >
                                                   <button type="submit" name="delete" class="btn btn-sm btn-danger text-white ms-2" onclick="confirm('Are You Sure??')" ><i class="fas fa-trash"></i></button>
                                               </form>
                                           </td>
                                       </tr>
                                         <?php
                                         }
                                         ?>
                                       </tbody>


                                    </table>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include('includs/footer.php'); ?>
    </div>
</div>
<?php include('includs/script.php') ?>

</body>

</html>