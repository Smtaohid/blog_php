<?php
session_start();
if (isset($_SESSION['email'])) {
    header('Location:index.php');
}

include('../Controller/AuthController.php');
if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $auth = new AuthController();
    $login =  $auth->login($email, $password);

    if ($login) {
        $_SESSION['email'] = $email;
        header('Location:index.php');
    } else {
        $_SESSION['msg'] = 'Email or Password Doesn\'t Match ';
    }
}
?>


<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>LogIn | Tech Learn</title>
    <style>
        .login-col {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);

        }

        p {
            margin: 5px;
            text-align: center;
        }

        a {
            text-decoration: none;
            color: darkcyan;
        }
    </style>

</head>

<body>

    <div class="container-fluid bg-dark vh-100">
        <div class="row justify-content-center position-relative vh-100">
            <div class="col-md-4 login-col">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>LogIn</h2>
                    </div>
                    <div class="card-body form-floating">
                        <?php
                        if (isset($_SESSION['msg'])) {

                        ?>
                            <div class="alert alert-danger"><?php echo $_SESSION['msg'] ?></div>
                        <?php
                            // unset($_SESSION['msg']); 
                        }
                        ?>
                        <form action="#" method="POST">
                            <label for="email" class="w-100">
                                Email:
                                <input class="form-control" type="email" name="email" id="email" placeholder="Emter Your Email">
                            </label>
                            <label for="password" class="w-100 my-3">
                                Password:
                                <input class="form-control" type="password" name="password" id="password" placeholder="Emter Your Password">
                            </label>

                            <input class="form-control btn btn-success" type="submit" name="submit" id="submit" value="LogIn">



                        </form>

                        <p><small> Password? <a href="#">Reset Here</small></a></p>
                        <p><small> Registered Yet? <a href="register.php">Register Here</small></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>