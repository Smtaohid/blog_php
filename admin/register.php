<?php
include('../Controller/AuthController.php');
if (isset($_POST['submit'])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $password = md5($_POST['password']);
    $role = 0;
    $query = "INSERT INTO users(name,email,phone,password,role) VALUES('$name','$email','$phone','$password','$role')";
    $auth = new AuthController();
    $auth->register($query);
}

?>


<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Register | Tech Learn</title>
    <style>
        .login-col {
            position: absolute;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
        }

        p {
            margin: 5px;
            text-align: center;
        }

        a {
            text-decoration: none;
            color: darkcyan;
        }
    </style>

</head>

<body>

    <div class="container-fluid bg-dark vh-100">
        <div class="row justify-content-center position-relative vh-100">
            <div class="col-md-4 login-col">
                <div class="card">
                    <div class="card-header text-center">
                        <h2>Register</h2>
                    </div>
                    <div class="card-body form-floating">
                        <form action="#" method="POST">
                            <label for="name" class="w-100">
                                Name:
                                <input class="form-control" type="text" name="name" id="name" placeholder="Emter Your Name">
                            </label>

                            <label for="email" class="w-100 mt-3">
                                Email:
                                <input class="form-control" type="email" name="email" id="email" placeholder="Emter Your Email">
                            </label>
                            <label for="phone" class="w-100 mt-3">
                                Phone:
                                <input class="form-control" type="number" name="phone" id="phone" placeholder="Emter Your Phone No.">
                            </label>
                            <label for="password" class="w-100 mt-3">
                                Password:
                                <input class="form-control" type="password" name="password" id="password" placeholder="Emter Your Password">
                            </label>

                            <input class="form-control btn btn-success mt-3" type="submit" name="submit" id="submit" value="Register">
                        </form>


                        <p><small> Registered Yet? <a href="login.php">Login Here</small></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>

</html>