<?php

include('../Controller/DatabaseFunction.php');


if (!isset($_GET['id'])){
    header('Location:catLists.php');
}
$id = $_GET['id'];

$singleCategory = "SELECT * FROM categories WHERE id=$id";
$functionObj = new  DatabaseFunction();
$result =  $functionObj->show($singleCategory);


if (isset($_POST['update'])) {
    $name = $_POST['cat_name'];
    $updateQuery = "UPDATE categories SET name='$name' WHERE  id=$id";
    $functionObj->update($updateQuery);
    header('Location:catLists.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php

    include('includs/head.php');

    ?>
</head>

<body class="sb-nav-fixed">
<nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
    <?php

    include('includs/nav.php');

    ?>
</nav>
<div id="layoutSidenav">
    <div id="layoutSidenav_nav">
        <?php include('includs/site_nav.php'); ?>
    </div>
    <div id="layoutSidenav_content">
        <main>
            <div class="container-fluid px-4">
                <h1 class="mt-4">Edit Category</h1>
                <ol class="breadcrumb mb-4">
                    <li class="breadcrumb-item active">Edit Category</li>
                </ol>
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="card shadow">
                                <div class="card-header">
                                    <h3>Edit Category</h3>
                                </div>
                                <div class="card-body">
                                    <form action="" method="POST" class="form">
                                        <input type="text" name="cat_name" value="<?php echo $result->name; ?>" class="form-control" placeholder="Enter Category Name">
                                        <button type="submit" name="update" class="btn btn-outline-success form-control mt-4">Create New Category</button>
                                    </form>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php include('includs/footer.php'); ?>
    </div>
</div>
<?php include('includs/script.php') ?>

</body>

</html>