<?php 

class DbConnection{
private $serverName = 'localhost';
private $userName = 'root';
private $password = '';
private $dbName = 'db_blog';


public $connection = null;

public function dbConnection()
{
    try {
        $this->connection = new PDO("mysql:host={$this->serverName};dbname={$this->dbName}",$this->userName,$this->password);
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
       return $this->connection;
    } catch (PDOException $e) {
    return $e->getMessage();
    }
}

}

?>