<?php
include('DbConnection.php');
class DatabaseFunction
{
    public $connection;

    public function __construct()
    {
        $connection = new DbConnection();
        $this->connection = $connection->dbConnection();
    }

    public function index($query)
    {

        $statement = $this->connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetchAll();
    }
    /**
     * @param $query
     * @return void
     */
    public function store($query)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();
    }

    /**
     * @param $query
     * @return void
     */
    public function update($query)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();
    }

    public function show($query)
    {

        $statement = $this->connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_OBJ);
        return $statement->fetch();
    }


    /**
     * @param $query
     * @return void
     */
    public function delete($query)
    {
        $statement = $this->connection->prepare($query);
        $statement->execute();
    }

}
