<?php

include('../Controller/DatabaseFunction.php');

if (isset($_POST['cat_submit'])) {
    $name = $_POST['cat_name'];
    $query = "INSERT INTO categories(name) VALUES('$name')";
    $functionObj = new DatabaseFunction();
    $functionObj->store($query);
    header('Location:catLists.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php

    include('includs/head.php');

    ?>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-dark">
        <?php

        include('includs/nav.php');

        ?>
    </nav>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <?php include('includs/site_nav.php'); ?>
        </div>
        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid px-4">
                    <h1 class="mt-4">Create Category</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Create Category</li>
                    </ol>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 mx-auto">
                                <div class="card shadow">
                                    <div class="card-header">
                                        <h3>Create New Category</h3>
                                    </div>
                                    <div class="card-body">
                                        <form action="" method="POST" class="form">
                                            <input type="text" name="cat_name" class="form-control" placeholder="Enter Category Name">
                                            <button type="submit" name="cat_submit" class="btn btn-outline-success form-control mt-4">Create New Category</button>
                                        </form>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <?php include('includs/footer.php'); ?>
        </div>
    </div>
    <?php include('includs/script.php') ?>

</body>

</html>